<?php

/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 20.05.17
 * Time: 20:08
 */
class MainChecker
{

    const DISHES_DELTA_THRESHOLD = 500;

    private $dateFrom;

    private $dateTo;

    private $dbConnection;

    private $reserves;

    private $arrangement;

    private $flights = [];

    public function __construct($dateFrom, $dateTo, $dbConnection)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->dbConnection = $dbConnection;

        $this->getReserves();
        $this->getArrangement();
    }

    private function getReserves()
    {
        $results = $this->dbConnection->query('SELECT flight_number, c_reserve, y_reserve FROM meal_schedule;');
        while ($row = $results->fetchArray()) {
            $this->reserves[$row['flight_number']] = [
                'c_reserve' => $row['c_reserve'],
                'y_reserve' => $row['y_reserve']
            ];
        }
    }

    private function getArrangement()
    {
        $results = $this->dbConnection->query('SELECT aircraft_number, c_amount, y_amount FROM arrangement_aircraft;');
        while ($row = $results->fetchArray()) {
            $this->arrangement[str_replace('-', '', $row['aircraft_number'])] = [
                'c_amount' => $row['c_amount'],
                'y_amount' => $row['y_amount']
            ];
        }
    }

    private function getPassengersWithSets()
    {
        $checkPassengersWithSets = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/queries/check_passengers_with_sets.sql');
        $stmtCheckPassengersWithSets = $this->dbConnection->prepare($checkPassengersWithSets);
        $stmtCheckPassengersWithSets->bindValue(':date_from', $this->dateFrom, SQLITE3_TEXT);
        $stmtCheckPassengersWithSets->bindValue(':date_to', $this->dateTo, SQLITE3_TEXT);

        $resultCheckPassengersWithSets = $stmtCheckPassengersWithSets->execute();

        if ($resultCheckPassengersWithSets->numColumns()) {
            while ($row = $resultCheckPassengersWithSets->fetchArray(SQLITE3_ASSOC)) {
                $flightKey = str_replace(['С7', 'ГЛ'], '', $row['Flight number']).'_'.$row['date_plan'];

                $this->flights[$flightKey] = [
                    'number' => $row['Flight number'],
                    'aircraft' => $row['aircraft_number'],
                    'date_plan' => $row['date_plan'],
                    'date_fact' => $row['Flight date fact'],
                    'Econom' => [
                        'amount_passengers' => $row['Econom passengers amount'],
                        'amount_sets' => $row['Econom sets amount'],
                        'verdict' => true,
                        'errors' => []
                    ],
                    'Business' => [
                        'amount_passengers' => $row['Business passengers amount'],
                        'amount_sets' => $row['Business sets amount'],
                        'verdict' => true,
                        'errors' => []
                    ],
                    'Crew' => [
                        'amount_people' => $row['Crew amount'],
                        'amount_sets' => $row['Crew sets amount'],
                        'verdict' => true,
                        'errors' => []
                    ]
                ];
            }
        }

        $stmtCheckPassengersWithSets->close();
    }

    private function getWeekNumber()
    {
        $checkWeekNumber = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/queries/check_week_number.sql');
        $stmtCheckWeekNumber = $this->dbConnection->prepare($checkWeekNumber);
        $stmtCheckWeekNumber->bindValue(':date_from', $this->dateFrom, SQLITE3_TEXT);
        $stmtCheckWeekNumber->bindValue(':date_to', $this->dateTo, SQLITE3_TEXT);

        $resultCheckWeekNumber = $stmtCheckWeekNumber->execute();

        if ($resultCheckWeekNumber->numColumns()) {
            while ($row = $resultCheckWeekNumber->fetchArray(SQLITE3_ASSOC)) {
                $flightKey = str_replace(['С7', 'ГЛ'], '', $row['Flight number']).'_'.$row['date_plan'];
                if (isset($this->flights[$flightKey])){
                    $this->flights[$flightKey]['Econom']['set_to_be'] = $row['set_econom_to_be'];
                    $this->flights[$flightKey]['Econom']['set_fact'] = $row['set_econom_fact'];
                    $this->flights[$flightKey]['Econom']['set_delta'] = $row['econom_delta'];

                    $this->flights[$flightKey]['Business']['set_to_be'] = $row['set_business_to_be'];
                    $this->flights[$flightKey]['Business']['set_fact'] = $row['set_business_fact'];
                    $this->flights[$flightKey]['Business']['set_delta'] = $row['business_delta'];

                    $this->flights[$flightKey]['Crew']['set_to_be'] = $row['set_crew_to_be'];
                    $this->flights[$flightKey]['Crew']['set_fact'] = $row['set_crew_fact'];
                    $this->flights[$flightKey]['Crew']['set_delta'] = $row['crew_delta'];

                }
            }
        }

        $stmtCheckWeekNumber->close();
    }

    private function getSetDish()
    {
        $checkSetDish = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/queries/check_set_dish.sql');
        $stmtCheckSetDish = $this->dbConnection->prepare($checkSetDish);
        $stmtCheckSetDish->bindValue(':date_from', $this->dateFrom, SQLITE3_TEXT);
        $stmtCheckSetDish->bindValue(':date_to', $this->dateTo, SQLITE3_TEXT);

        $resultCheckSetDish = $stmtCheckSetDish->execute();

        if ($resultCheckSetDish->numColumns()) {
            while ($row = $resultCheckSetDish->fetchArray(SQLITE3_ASSOC)) {
                $flightKey = str_replace(['С7', 'ГЛ'], '', $row['Flight number']).'_'.$row['date_plan'];

                if (isset($this->flights[$flightKey])){

                    $businessDishesToBe = explode(',', $row['c_dishes_to_be']);
                    sort($businessDishesToBe);
                    $businessDishesFact = explode(',', $row['c_dishes_fact']);
                    sort($businessDishesFact);

                    $this->flights[$flightKey]['Business']['dishes_to_be'] = $businessDishesToBe;
                    $this->flights[$flightKey]['Business']['dishes_fact'] = $businessDishesFact;
                    $this->flights[$flightKey]['Business']['dishes_to_be_sum'] = $row['c_dishes_to_be_sum'];
                    $this->flights[$flightKey]['Business']['dishes_fact_sum'] = $row['c_dishes_sum'];
                    $this->flights[$flightKey]['Business']['dishes_delta'] = $row['c_delta'];

                    $crewDishesToBe = explode(',', $row['k_dishes_to_be']);
                    sort($crewDishesToBe);
                    $crewDishesFact = explode(',', $row['k_dishes_fact']);;
                    sort($crewDishesFact);
                    $this->flights[$flightKey]['Crew']['dishes_to_be'] = $crewDishesToBe;
                    $this->flights[$flightKey]['Crew']['dishes_fact'] = $crewDishesFact;
                    $this->flights[$flightKey]['Crew']['dishes_to_be_sum'] = $row['k_dishes_to_be_sum'];
                    $this->flights[$flightKey]['Crew']['dishes_fact_sum'] = $row['k_dishes_fact_sum'];
                    $this->flights[$flightKey]['Crew']['dishes_delta'] = $row['k_delta'];
                }
            }
        }

        $stmtCheckSetDish->close();
    }

    private function ruleAmountPassengers(&$flight, $key)
    {

        if ($flight['Econom']['amount_sets'] <= max($flight['Econom']['amount_passengers'] + $this->reserves[$flight['number']], $this->arrangement[$flight['aircraft']])) {
            return;
        }

        $flight['verdict'] = false;
        $flight['Econom']['errors'][] = 'Количество комплектов эконома больше положенного';


    }

    private function ruleWeek(&$flight, $key)
    {
        if (isset($flight['Econom']['set_fact']) && isset($flight['Econom']['set_to_be']) && $flight['Econom']['set_to_be'] != $flight['Econom']['set_fact']) {
            $flight['Econom']['verdict'] = false;
            $flight['Econom']['errors'][] = 'Комплект эконома не верен';
        }

        if (isset($flight['Business']['set_fact']) && isset($flight['Business']['set_to_be']) && $flight['Business']['set_to_be'] != $flight['Business']['set_fact']) {
            $flight['Business']['verdict'] = false;
            $flight['Business']['errors'][] = 'Комплект бизнес-класса не верен';
        }

        if (isset($flight['Crew']['set_fact']) && isset($flight['Crew']['set_to_be']) && str_replace('К', 'K', $flight['Crew']['set_to_be']) != str_replace('К', 'K', $flight['Crew']['set_fact'])) {

            $flight['Crew']['verdict'] = false;
            $flight['Crew']['errors'][] = 'Комплект экипажа не верен';
        }
    }

    private function ruleCheckDishes(&$flight, $key)
    {
        if ($flight['Business']['amount_passengers'] > 0 && abs($flight['Business']['dishes_delta']) >= self::DISHES_DELTA_THRESHOLD) {

            if (!empty($flight['Business']['dishes_to_be']) &&
                !empty($flight['Business']['dishes_fact']) &&
                !empty(array_diff(array_filter($flight['Business']['dishes_to_be']), array_filter($flight['Business']['dishes_fact'])))) {
                $flight['Business']['verdict'] = false;
                $flight['Business']['errors'][] = 'Стоимость блюд бизнес-класса некорректна';
                $flight['Business']['errors'][] = 'Неправильный набор блюд для бизнес-класса';
            }
        }

//        if (!empty(array_diff($flight['Crew']['dishes_to_be'], $flight['Crew']['dishes_fact']))) {
//            $flight['verdict'] = false;
//            $flight['errors'][] = 'Неправильный набор блюд для бизнес-класса';
//        }

    }

    public function importData()
    {
        $this->getPassengersWithSets();
        $this->getWeekNumber();
        $this->getSetDish();
    }

    public function check()
    {

        $rules = [
            'ruleAmountPassengers',
            'ruleWeek',
            'ruleCheckDishes'
        ];

        foreach ($rules as $rule) {
            array_walk($this->flights, array('self', $rule));
        }

        foreach ($this->flights as $key => $flight) {
            $failesRows = 3;
            if ($flight['Econom']['verdict']) {
                unset($this->flights[$key]['Econom']);
                $failesRows--;
            }
            if ($flight['Business']['verdict']) {
                unset($this->flights[$key]['Business']);
                $failesRows--;
            }
            if ($flight['Crew']['verdict']) {
                unset($this->flights[$key]['Crew']);
                $failesRows--;
            }
            $this->flights[$key]['rows'] = $failesRows;
        }

        $this->flights = array_filter($this->flights, function($flight){
            return $flight['rows'] > 0;
        });
    }

    public function saveAnalysis()
    {
        $tableTitle = 'analysis'. str_replace("-", "", $this->dateFrom).'_'. str_replace("-", "", $this->dateTo);

        $deleteTable = 'DROP TABLE IF EXISTS '.$tableTitle;

        $this->dbConnection->query($deleteTable);

        $createTableSql = 'CREATE TABLE "'.$tableTitle.'" (
          "flight_number" TEXT NOT NULL,
          "aircraft" TEXT NOT NULL,
          "date_plan" TEXT NOT NULL,
          "date_fact" TEXT,
          "econom_amount_passengers" INTEGER,
          "econom_sets" INTEGER,
          "econom_set_to_be" TEXT,
          "econom_set_fact" TEXT,
          "econom_set_delta" REAL,
          "econom_verdict" BOOLEAN,
          "econom_errors" TEXT,
          "business_amount_passengers" INTEGER,
          "business_sets" INTEGER,
          "business_set_to_be" TEXT,
          "business_set_fact" TEXT,
          "business_set_delta" REAL,
          "business_dishes_to_be" TEXT,
          "business_dishes_fact" TEXT,
          "business_dishes_to_be_sum" REAL,
          "business_dishes_fact_sum" REAL,
          "business_dishes_delta" REAL,
          "business_verdict" BOOLEAN,
          "business_errors" TEXT,
          "crew_amount_passengers" INTEGER,
          "crew_sets" INTEGER,
          "crew_set_to_be" TEXT,
          "crew_set_fact" TEXT,
          "crew_set_delta" REAL,
          "crew_dishes_to_be" TEXT,
          "crew_dishes_fact" TEXT,
          "crew_dishes_to_be_sum" REAL,
          "crew_dishes_fact_sum" REAL,
          "crew_dishes_delta" REAL,
          "crew_verdict" BOOLEAN,
          "crew_errors" TEXT
        );';


        $this->dbConnection->query($createTableSql);

        $insertPrepare = $this->dbConnection->prepare('INSERT INTO '.$tableTitle.' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
        foreach ($this->flights as $flight) {

            $insertPrepare->bindParam(1, $flight['number'], SQLITE3_TEXT);
            $insertPrepare->bindParam(2, $flight['aircraft'], SQLITE3_TEXT);
            $insertPrepare->bindParam(3, $flight['date_plan'], SQLITE3_TEXT);
            $insertPrepare->bindParam(4, $flight['date_fact'], SQLITE3_TEXT);
            $insertPrepare->bindParam(5, $flight["Econom"]['amount_passengers'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(6, $flight["Econom"]['amount_sets'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(7, $flight["Econom"]['set_to_be'], SQLITE3_TEXT);
            $insertPrepare->bindParam(8, $flight["Econom"]['set_fact'], SQLITE3_TEXT);
            $insertPrepare->bindParam(9, $flight['Econom']["set_delta"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(10, !isset($flight['Econom']["verdict"]) ? 1 : $flight['Econom']["verdict"], SQLITE3_INTEGER);
            $insertPrepare->bindParam(11, !empty($flight['Econom']["errors"]) ? implode(',', $flight['Econom']["errors"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(12, $flight["Business"]['amount_passengers'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(13, $flight["Business"]['amount_sets'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(14, $flight["Business"]['set_to_be'], SQLITE3_TEXT);
            $insertPrepare->bindParam(15, $flight["Business"]['set_fact'], SQLITE3_TEXT);
            $insertPrepare->bindParam(16, $flight['Business']["set_delta"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(17, !empty($flight['Business']["dishes_to_be"]) ? implode(',', $flight['Business']["dishes_to_be"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(18, !empty($flight['Business']["dishes_fact"]) ?  implode(',', $flight['Business']["dishes_fact"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(19, $flight['Business']["dishes_to_be_sum"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(20, $flight['Business']["dishes_fact_sum"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(21, $flight['Business']["dishes_delta"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(22, !isset($flight['Business']["verdict"]) ? 1 : $flight['Business']["verdict"], SQLITE3_INTEGER);

            $insertPrepare->bindParam(23, !empty($flight['Business']["errors"]) ? implode(',', $flight['Business']["errors"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(24, $flight["Crew"]['amount_people'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(25, $flight["Crew"]['amount_sets'], SQLITE3_INTEGER);
            $insertPrepare->bindParam(26, $flight["Crew"]['set_to_be'], SQLITE3_TEXT);
            $insertPrepare->bindParam(27, $flight["Crew"]['set_fact'], SQLITE3_TEXT);
            $insertPrepare->bindParam(28, $flight['Crew']["set_delta"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(29, !empty($flight['Crew']["dishes_to_be"]) ? implode(',', $flight['Crew']["dishes_to_be"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(30, !empty($flight['Crew']["dishes_fact"]) ? implode(',', $flight['Crew']["dishes_fact"]) : null, SQLITE3_TEXT);
            $insertPrepare->bindParam(31, $flight['Crew']["dishes_to_be_sum"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(32, $flight['Crew']["dishes_fact_sum"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(33, $flight['Crew']["dishes_delta"], SQLITE3_FLOAT);
            $insertPrepare->bindParam(34, !isset($flight['Crew']["verdict"]) ? 1 : $flight['Crew']["verdict"], SQLITE3_INTEGER);
            $insertPrepare->bindParam(35, !empty($flight['Crew']["errors"]) ? implode(',', $flight['Crew']["errors"]) : null, SQLITE3_TEXT);

            $insertPrepare->execute();

            $insertPrepare->clear();

        }
    }


}