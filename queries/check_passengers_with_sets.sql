SELECT
  y.flight_number 'Flight number',
  date_plan_loc 'date_plan',
  aircraft_number,
  DATE(datetime_departure_fact_loc) 'Flight date fact',
  y_amount 'Econom passengers amount',
  y.amount + coalesce(yr.amount, 0) 'Econom sets amount',
  c_amount 'Business passengers amount',
  c.amount + coalesce(cr.amount, 0) 'Business sets amount',
  k_amount 'Crew amount',
  k.amount 'Crew sets amount'
FROM flight_schedule
LEFT JOIN (
    SELECT
      SUM(amount) 'amount',
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Эконом'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) y ON flight_schedule.date_plan_loc = y.date_plan AND (y.flight_number LIKE 'С7' || flight_schedule.flight_number OR y.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
LEFT JOIN (
    SELECT
      SUM(SUBSTR(REPLACE(classification_name, '*', ''), instr(classification_name, 'R') + 1)) * amount 'amount',
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND flight_class = 'Эконом-спец' -- ищем резервные копии
    GROUP BY flight_number, DATE(departure_datetime_plan)
) yr ON flight_schedule.date_plan_loc = yr.date_plan AND (yr.flight_number LIKE 'С7' || flight_schedule.flight_number OR yr.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
LEFT JOIN (
    SELECT
      SUM(amount) 'amount',
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Бизнес'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) c ON flight_schedule.date_plan_loc = c.date_plan AND (c.flight_number LIKE 'С7' || flight_schedule.flight_number OR c.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
LEFT JOIN (
    SELECT
      SUM(SUBSTR(REPLACE(classification_name, '*', ''), instr(classification_name, 'R') + 1)) * amount 'amount',
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND flight_class = 'Бизнес-спец' -- ищем резервные копии
    GROUP BY flight_number, DATE(departure_datetime_plan)
) cr ON flight_schedule.date_plan_loc = cr.date_plan AND (cr.flight_number LIKE 'С7' || flight_schedule.flight_number OR cr.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
LEFT JOIN (
    SELECT
      SUM(amount) 'amount',
      flight_number,
      DATE(departure_datetime_plan) 'date_plan'
    FROM
      reestr_dks
    WHERE
      classification_type='Комплект' AND
      flight_class = 'Экипаж ВС'
    GROUP BY flight_number, DATE(departure_datetime_plan)
) k ON flight_schedule.date_plan_loc = k.date_plan AND (k.flight_number LIKE 'С7' || flight_schedule.flight_number OR k.flight_number LIKE 'ГЛ' || flight_schedule.flight_number)
WHERE date_plan_loc BETWEEN :date_from AND :date_to and y.flight_number IS NOT NULL