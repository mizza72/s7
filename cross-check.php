<?php

    include('classes/MainChecker.php');
    $dbConnection = new SQLite3($_SERVER['DOCUMENT_ROOT'].'/s7');

    $checker = new MainChecker($_GET['date_from'], $_GET['date_to'], $dbConnection);

    $checker->importData();
    $checker->check();
    $checker->saveAnalysis();
    echo "ok";