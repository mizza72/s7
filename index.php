<?php
/**
 * Created by PhpStorm.
 * User: pavelmyznikov
 * Date: 20.05.17
 * Time: 16:55
 */
?>

<?php include('template/header.php'); ?>

<div class="starter-template">
    <h1 style="color: #666">Проверка загрузки питания ВС</h1>
    <p class="lead">Сверка данных от контрагента по загрузке питанием воздушных судов за 2016-2017 годы</p>
</div>


<table class="table table-striped">
    <thead>
    <tr>
        <th style="width: 40%;">Период <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></th>
        <th style="width: 30%;">Данные <span class="glyphicon glyphicon-plane" aria-hidden="true"></span></th>
        <th style="width: 25%">Статус <span class="glyphicon glyphicon-stats" aria-hidden="true"></span></th>
        <th style="width: 5%"></th>
        <!--<th>Username</th>-->
    </tr>
    </thead>
    <tbody>
    <tr class="success zagrug">
        <th scope="row">1-10 Декабрь 2016</th>
        <td>Загружено</td>
        <td>Проверено <span class="glyphicon glyphicon-ok cc" aria-hidden="true"></span>
        </td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr class="success">
        <th scope="row">11-20 Декабрь 2016</th>
        <td>Загружено</td>
        <td>Проверено
            <span class="glyphicon glyphicon-ok cc" aria-hidden="true"></span>
        </td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr class="success">
        <th scope="row">21-31 Декабрь 2016</th>
        <td>Загружено</td>
        <td>Проверено
            <span class="glyphicon glyphicon-ok cc" aria-hidden="true"></span>
        </td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr class="warning">
        <th scope="row">1-10 Января 2017</th>
        <td>Загружено</td>
        <td>Обработано, ожидает подтверждения
            <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
        </td>
        <td><a href="check/?date_from=2017-01-01&date_to=2017-01-10"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr class="warning">
        <th scope="row">11-20 Января 2017</th>
        <td>
            Загружено
        </td>
        <td>Обработано, ожидает подтверждения
            <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>
        </td>
        <td><a href="check/?date_from=2017-01-11&date_to=2017-01-20"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr>
        <th scope="row">21-31 Января 2017</th>
        <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Загрузить
            </button>
        </td>
        <td>Не обработано</td>
        <td><a href="check/?date_from=2017-01-21&date_to=2017-01-31"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr>
        <th scope="row">1-10 Февраля 2017</th>
        <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Загрузить
            </button>
        </td>
        <td>Не обработано</td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr>
        <th scope="row">11-20 Февраля 2017</th>
        <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Загрузить
            </button>
        </td>
        <td>Не обработано</td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    <tr>
        <th scope="row">21-28 Февраля 2017</th>
        <td>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                Загрузить
            </button>
        </td>
        <td>Не обработано</td>
        <td><a href=""><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
    </tr>
    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Загрузка данных от контрагента</h4>
            </div>
            <div class="modal-body">
                <form id="import-form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Примечания</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Опционально">
                    </div>
                    <!--<div class="form-group">-->
                    <!--<label for="exampleInputPassword1">Password</label>-->
                    <!--<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">-->
                    <!--</div>-->
                    <div class="form-group">
                        <label for="exampleInputFile">Выберите файл реестра от контрагента</label>
                        <input type="file" id="input_file">
                        <!--<p class="help-block">Example block-level help text here.</p>-->
                    </div>
                    <!--<div class="checkbox">-->
                    <!--<label>-->
                    <!--<input type="checkbox"> Check me out-->
                    <!--</label>-->
                    <!--</div>-->
                    <button type="submit" id="import-submit" class="btn btn-default">Отправить</button>
                    <span id="import-info"></span>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary">Загрузить</button>
            </div>
        </div>
    </div>
</div>


<?php include('template/footer.php')?>
